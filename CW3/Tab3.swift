//
//  Tab3.swift
//  CW3
//
//  Created by Vasileios Katziouras [sc13vk] on 04/12/2015.
//
//

import UIKit
import WebKit

/**
Tab3

Tab that displays a google search for the chosen company
*/
class Tab3: UIViewController {
    
    //Loading variables needed
    var url: NSURL?
    var company = Company()

    override func viewDidLoad() {
        super.viewDidLoad()
        let reachability = Reachability.reachabilityForInternetConnection()
        reachability.startNotifier()
        
        if reachability.isReachable(){
            
            reachability.stopNotifier()
            //Constructing the url string
            var x = "https://www.google.co.uk/#q="+company.companyName;
            let newString = x.stringByReplacingOccurrencesOfString(
                " ", withString: "+",
                options: NSStringCompareOptions.LiteralSearch, range: nil)
            url = NSURL(string:newString);
            var req = NSURLRequest(URL: url!)
            //Making the url request
            self.webView!.loadRequest(req)
        }
        else{
            let alert = UIAlertController(title: "Network Error",
                message: "Please check your internet connection",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let alertAction = UIAlertAction(title: "Ok",
                style: UIAlertActionStyle.Default)
                { (UIAlertAction) -> Void in }
            
            alert.addAction(alertAction)
            presentViewController(alert, animated: true) { () -> Void in }
        }
        reachability.stopNotifier()

    }
    
    // MARK: - Outlet
    @IBOutlet weak var webView: UIWebView!{
        didSet {
            if url != nil {
                let request = NSURLRequest(URL: url!)
                webView.loadRequest(request)
            }
            
        }
    }
}
