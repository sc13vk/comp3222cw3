//
//  TableViewCell.swift
//  CW3
//
//  Created by Attila Gergely Eke [sc13age] on 05/12/2015.
//
//

import UIKit

/**
TableViewCell

Class of the cells of the table view
*/
class TableViewCell: UITableViewCell {
    
    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    
}
