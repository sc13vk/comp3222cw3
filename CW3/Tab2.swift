//
//  Tab2.swift
//  CW3
//
//  Created by Vasileios Katziouras [sc13vk] on 04/12/2015.
//
//

import UIKit
import MapKit
import CoreLocation
import AddressBook

/**
Tab2

This tab displays the location of the company on map using geocoder
*/
class Tab2: UIViewController {
    
    //Loading data
    var company = Company()
    
    // MARK: - Outlet
    @IBOutlet weak var mapView: MKMapView!{
        didSet {
            let reachability = Reachability.reachabilityForInternetConnection()
            reachability.startNotifier()
            
            if reachability.isReachable(){
                mapView.mapType = .Standard
                mapView.pitchEnabled = false
                
                //Extracting coordinates from physical address if possible
                var geocoder = CLGeocoder();
                
                geocoder.geocodeAddressString(company.address,
                    completionHandler: { placemarks, error in
                    if let placemark = placemarks.first as? CLPlacemark {
                        
                        let location = CLLocationCoordinate2DMake(
                            placemark.location.coordinate.latitude,
                            placemark.location.coordinate.longitude)
                        
                        let region = MKCoordinateRegionMakeWithDistance(
                            location, 1000.0, 1000.0)
                        
                        let dropPin = MKPointAnnotation()
                        
                        dropPin.coordinate = location
                        dropPin.title = self.company.companyName
                        dropPin.subtitle = self.company.addressLine1 + ", " +
                        self.company.locality + ", " + self.company.postalCode
                        self.mapView.addAnnotation(dropPin)
                        //Displaying Location of the chosen company in the map
                        self.mapView.setRegion(region, animated: true)
                    }
                })

            }
            else{
                let alert = UIAlertController(title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: UIAlertControllerStyle.Alert)
                
                let alertAction = UIAlertAction(title: "Ok",
                    style: UIAlertActionStyle.Default)
                    { (UIAlertAction) -> Void in }
                
                alert.addAction(alertAction)
                presentViewController(alert, animated: true) { () -> Void in }
            }
            reachability.stopNotifier()

        }
    }
}
