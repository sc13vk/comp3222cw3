//
//  TableViewController.swift
//  CW3
//
//  Created by Attila Gergely Eke [sc13age] on 03/12/2015.
//
//

import UIKit
import Alamofire

/**
TableViewController

Shows a list of selectable companies
*/
class TableViewController: UITableViewController {
    
    // MARK: - Fields
    var TableData:Array<Company> = Array<Company>()
    var companyName = ""
    var object = Company()
    
    // MARK: - Actions
    /**
    viewDidLoad
    
    This function's triggered when the view's loaded.
    If we have internet connection then we do a GET request, parse the result
    and save it in a Company instance that we later add to an array.
    If there's no internet connection we show an alert.
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let reachability = Reachability.reachabilityForInternetConnection()
        
        reachability.startNotifier()

        if reachability.isReachable(){
        
            let headers = [
                "Authorization": "Basic MWJmTVBQQkpGbE1vVGhVSnY2MDZTaUZuUnlVM3ZhdlVORFFjTnptUg==",
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(.GET,
                "https://api.companieshouse.gov.uk/search" +
                "/companies?q=\(self.companyName)", headers: headers)
                .responseJSON { _, _, data, _ in
                    let json = JSON(data!)
                    
                    for (key, subJson) in json["items"] {
                        
                        let inst = Company()
                        
                        if let title = subJson["title"].string {
                            inst.companyName = title
                        }
                        
                        if let companyNumber = subJson["company_number"].string {
                            inst.companyNumber = companyNumber
                        }
                        
                        if let address =
                            subJson["snippet"].string {
                                inst.address = address
                        }
                        
                        if let description =
                            subJson["description"].string {
                                inst.description = description
                        }
                        
                        if let companyType =
                            subJson["company_type"].string {
                                inst.companyType = companyType
                        }
                        
                        if let dateOfCreation =
                            subJson["date_of_creation"].string {
                                inst.dateOfCreation = dateOfCreation
                        }
                        
                        if let companyStatus =
                            subJson["company_status"].string {
                                inst.companyStatus = companyStatus
                        }
                        
                        if let addressLine1 =
                            subJson["address"]["address_line_1"].string {
                                inst.addressLine1 = addressLine1
                        }
                        
                        if let addressLine2 =
                            subJson["address"]["address_line_2"].string {
                                inst.addressLine2 = addressLine2
                        }
                        
                        if let country =
                            subJson["address"]["country"].string {
                                inst.country = country
                        }
                        
                        if let locality =
                            subJson["address"]["locality"].string {
                                inst.locality = locality
                        }
                        
                        if let poBox =
                            subJson["address"]["po_box"].string {
                                inst.poBox = poBox
                        }
                        
                        if let postalCode =
                            subJson["address"]["postal_code"].string {
                                inst.postalCode = postalCode
                        }
                        
                        if let region =
                            subJson["address"]["region"].string {
                                inst.region = region
                        }
                        
                        self.TableData.append(inst)
                    }
                    
                    
                    self.do_table_refresh();
            }
            reachability.stopNotifier()

        }
        else {
            let alert = UIAlertController(title: "Network Error",
                message: "Please check your internet connection",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let alertAction = UIAlertAction(title: "Ok",
                style: UIAlertActionStyle.Default)
                { (UIAlertAction) -> Void in }
            
            alert.addAction(alertAction)
            presentViewController(alert, animated: true) { () -> Void in }
        }

    }
    
    /**
    do_table_refresh
    
    Reloads the data in the tableview in another thread
    */
    func do_table_refresh()
    {
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.reloadData()
            return
        })
    }

    // MARK: - Table view functions
    /**
    numberOfSectionsInTableView
    
    Returns the number of sections.
    
    :param: UITableView
    */
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    /**
    tableView
    
    Returns the number of rows in the section.
    
    :param: UITableView
    :param: Int
    */
    override func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int {
        return TableData.count
    }
    
    /**
    tableView
    
    Fills in the tableView's cells with the name of companies from an array.
    Plus puts a chevron to the right side of each cell.
    
    :param: UITableView
    :param: NSIndexPath
    */
    override func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            
        let cell = tableView.dequeueReusableCellWithIdentifier("cell",
            forIndexPath: indexPath) as! UITableViewCell
        
        
        object = TableData[indexPath.row] as Company
        cell.textLabel!.text = object.companyName
        
        let chevron = UIImage(named: "chevron.png")
        cell.accessoryType = .DisclosureIndicator
        cell.accessoryView = UIImageView(image: chevron!)
        
        return cell
    }

    // MARK: - Segue
    /**
    prepareForSegue
    
    Segue for the tabBarView that contains 3 views.
    It sends the data to all three views.
    
    :param: UIStoryboardSegue
    :param: AnyObject?
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == "ShowDetails")
        {
            var tabBarNav: TabBarController = segue.destinationViewController
                as! TabBarController
            
            var desView: Tab1 = tabBarNav.viewControllers?.first as! Tab1
            var desView2: Tab2 = tabBarNav.viewControllers?[1] as! Tab2
            var desView3: Tab3 = tabBarNav.viewControllers?.last as! Tab3

            
            let indexPath = self.tableView.indexPathForSelectedRow()!
            
            object = TableData[indexPath.row] as Company
            let titleString = object.companyName
            
            desView.company.companyName = object.companyName
            desView.company.companyNumber = object.companyNumber
            desView.company.address = object.address
            desView.company.description = object.description
            desView.company.companyType = object.companyType
            desView.company.dateOfCreation = object.dateOfCreation
            desView.company.companyStatus = object.companyStatus
            desView.company.addressLine1 = object.addressLine1
            desView.company.addressLine2 = object.addressLine2
            desView.company.country = object.country
            desView.company.locality = object.locality
            desView.company.poBox = object.poBox
            desView.company.postalCode = object.postalCode
            desView.company.region = object.region
            
            desView2.company.companyName = object.companyName
            desView2.company.companyNumber = object.companyNumber
            desView2.company.address = object.address
            desView2.company.description = object.description
            desView2.company.companyType = object.companyType
            desView2.company.dateOfCreation = object.dateOfCreation
            desView2.company.companyStatus = object.companyStatus
            desView2.company.addressLine1 = object.addressLine1
            desView2.company.addressLine2 = object.addressLine2
            desView2.company.country = object.country
            desView2.company.locality = object.locality
            desView2.company.poBox = object.poBox
            desView2.company.postalCode = object.postalCode
            desView2.company.region = object.region
            
            desView3.company.companyName = object.companyName
            desView3.company.companyNumber = object.companyNumber
            desView3.company.address = object.address
            desView3.company.description = object.description
            desView3.company.companyType = object.companyType
            desView3.company.dateOfCreation = object.dateOfCreation
            desView3.company.companyStatus = object.companyStatus
            desView3.company.addressLine1 = object.addressLine1
            desView3.company.addressLine2 = object.addressLine2
            desView3.company.country = object.country
            desView3.company.locality = object.locality
            desView3.company.poBox = object.poBox
            desView3.company.postalCode = object.postalCode
            desView3.company.region = object.region

        }
    }
}
