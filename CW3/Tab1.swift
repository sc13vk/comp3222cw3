//
//  Tab1.swift
//  CW3
//
//  Created by Vasileios Katziouras [sc13vk] on 04/12/2015.
//
//

import UIKit

/**
Tab1

This tab displays some basic information about the chosen company
*/
class Tab1: UITableViewController {
    
    // MARK: - Field
    var company = Company()
    
    // MARK: - Outlets
    @IBOutlet weak var companyName: UILabel!
        {
        didSet{
            if company.companyName != ""{
                companyName.text = company.companyName
            }
        }
    }
    
    @IBOutlet weak var companyNumber: UILabel!{
        didSet{
            if company.companyNumber != ""{
                companyNumber.text = company.companyNumber
            }
        }
    }
    
    @IBOutlet weak var companyDescription: UILabel!{
        didSet{
            if company.description != ""{
                companyDescription.text = company.description
            }
        }
    }
    
    @IBOutlet weak var companyStatus: UILabel!{
        didSet{
            if company.companyStatus != ""{
                companyStatus.text = company.companyStatus
            }
        }
    }
    
    @IBOutlet weak var companyAddressLine1: UILabel!{
        didSet{
            if company.addressLine1 != ""{
                companyAddressLine1.text = company.addressLine1
            }
        }
    }
    
    @IBOutlet weak var companyAddressLine2: UILabel!{
        didSet{
            if company.addressLine2 != ""{
                companyAddressLine2.text = company.addressLine2
            }
        }
    }
    
    @IBOutlet weak var companyCountry: UILabel!{
        didSet{
            if company.country != ""{
                companyCountry.text = company.country
            }
        }
    }
    
    @IBOutlet weak var companyLocality: UILabel!{
        didSet{
            if company.locality != ""{
                companyLocality.text = company.locality
            }
        }
    }
    
    @IBOutlet weak var companyPostalCode: UILabel!{
        didSet{
            if company.postalCode != ""{
                companyPostalCode.text = company.postalCode
            }
        }
    }
    
    @IBOutlet weak var companyRegion: UILabel!{
        didSet{
            if company.region != ""{
                companyRegion.text = company.region
            }
        }
    }
    @IBOutlet weak var companyDateOfCreation: UILabel!{
        didSet{
            if company.dateOfCreation != ""{
                companyDateOfCreation.text = company.dateOfCreation
            }
        }
    }
}
