//
//  ViewController.swift
//  CW3
//
//  Created by Attila Gergely Eke [sc13age] on 02/12/2015.
//
//

import UIKit
import Alamofire


/**
* ViewController
*
* The main view's controller
*/
class ViewController: UIViewController, UITextFieldDelegate {

    // MARK: - Field
    var searchQuery = ""
    
    // MARK: - Outlet
    @IBOutlet weak var companyTextBox: UITextField!
    
    
    // MARK: - Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        companyTextBox.delegate = self
        
        }
    
    /**
    textFieldShouldReturn
    
    This function's triggered when we press the search button on the keyboard.
    It saves the current text to a variable and then replaces the blank spaces
    with +s. We need this for the query on the next view.
    
    :param: UITextField
    
    */
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Hide the keyboard.
        searchQuery = companyTextBox.text
        searchQuery = searchQuery.stringByReplacingOccurrencesOfString(
            " ", withString: "+")
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Segue
    /**
    prepareForSegue
    
    We need this function for the transition and to check if we have internet
    access.
    It checks the segue identifier and the internet access. 
    If there isn't internet then it shows an alert. 
    If there is then we pass the typed phrase to the next view.
    
    :param: UIStoryboardSegue
    :param: AnyObject?
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SearchCompanies"{
            let reachability = Reachability.reachabilityForInternetConnection()
            
            reachability.startNotifier()
            
            if reachability.isReachable(){
                if let vc = segue.destinationViewController
                    as? TableViewController{
                    vc.companyName = searchQuery
                }
            }
            else{
                let alert = UIAlertController(
                    title: "Network Error",
                    message: "Please check your internet connection",
                    preferredStyle: UIAlertControllerStyle.Alert)
                
                let alertAction = UIAlertAction(title: "Ok",
                    style: UIAlertActionStyle.Default)
                    { (UIAlertAction) -> Void in }
                
                alert.addAction(alertAction)
                presentViewController(alert, animated: true) { () -> Void in }
            }
            reachability.stopNotifier()
        }
    }
}

