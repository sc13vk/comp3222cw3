//
//  Company.swift
//  CW3
//
//  Created by Attila Gergely Eke [sc13age] on 05/12/2015.
//
//

import Foundation

/**
Company

We parse the JSON data that we receive from the internet
to instances of this class.
*/
class Company {
    
    // MARK: - Fields    
    var companyName = ""
    var companyNumber = ""
    var address = ""
    var description = ""
    var companyType = ""
    var dateOfCreation = ""
    var companyStatus = ""
    var addressLine1 = ""
    var addressLine2 = ""
    var country = ""
    var locality = ""
    var poBox = ""
    var postalCode = ""
    var region = ""
    
}
